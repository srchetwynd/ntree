export interface RawDirectories {
    type: string;
    name: string;
    children?: RawDirectories[];
}

export interface FlatDirectories {
    type: string;
    name: string;
    depth: number;
}
