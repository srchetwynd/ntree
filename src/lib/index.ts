import fs from 'node:fs';
import path from 'node:path';

const CHARS = {
    continue: '├── ',
    end: '└── ',
    line: '│   ',
    empty: '    ',
};

const STARTS_WITH_DOT = new RegExp('^\\.');

function tre(
    directory: string,
    output: NodeJS.WriteStream,
    options: {
        depth: number;
        hidden: boolean;
        directoriesOnly: boolean;
        fullPath: boolean;
        pattern?: string;
        antiPattern?: string;
        fileLimit?: number;
    },
    prefix = ''
): { directories: number; files: number } {
    const directoryFiles = fs.readdirSync(directory, { withFileTypes: true });
    let directoryCount = 0;
    let fileCount = 0;
    let patternRegex;
    let antiPatternRegex;
    if (typeof options.pattern !== 'undefined') {
        patternRegex = new RegExp(options.pattern);
    }
    if (typeof options.antiPattern !== 'undefined') {
        antiPatternRegex = new RegExp(options.antiPattern);
    }

    if (
        typeof options.fileLimit !== 'undefined' &&
        directoryFiles.length > options.fileLimit &&
        options.hidden
    ) {
        output.write(
            ` [${directoryFiles.length} entries exceeds filelimit, not opening dir]`
        );
        return {
            directories: 0,
            files: 0,
        };
    }
    if (
        typeof options.fileLimit !== 'undefined' &&
        directoryFiles.filter((file) => STARTS_WITH_DOT.test(file.name))
            .length > options.fileLimit &&
        !options.hidden
    ) {
        output.write(
            ` [${
                directoryFiles.filter((file) => STARTS_WITH_DOT.test(file.name))
                    .length
            } entries exceeds filelimit, not opening dir]`
        );
        return {
            directories: 0,
            files: 0,
        };
    }

    for (let index = 0; index < directoryFiles.length; index++) {
        const file = directoryFiles[index];

        if (options.hidden === false && STARTS_WITH_DOT.test(file.name)) {
            continue;
        } else if (options.directoriesOnly && !file.isDirectory()) {
            continue;
        } else if (
            typeof patternRegex !== 'undefined' &&
            !file.isDirectory() &&
            !patternRegex.test(file.name)
        ) {
            continue;
        } else if (
            typeof antiPatternRegex !== 'undefined' &&
            !file.isDirectory() &&
            antiPatternRegex.test(file.name)
        ) {
            continue;
        }

        output.write(prefix);
        if (index === directoryFiles.length - 1) {
            output.write(CHARS.end);
        } else {
            output.write(CHARS.continue);
        }

        if (options.fullPath) {
            output.write(path.join(directory, file.name));
        } else {
            output.write(file.name);
        }
        output.write('\n');

        if (options.depth > 0 && file.isDirectory()) {
            directoryCount++;
            const results = tre(
                path.join(directory, file.name),
                output,
                {
                    ...options,
                    depth: options.depth - 1,
                },
                prefix +
                    (index === directoryFiles.length - 1
                        ? CHARS.empty
                        : CHARS.line)
            );

            directoryCount += results.directories;
            fileCount += results.files;
        } else {
            fileCount++;
        }
    }

    return {
        directories: directoryCount,
        files: fileCount,
    };
}

export default tre;
