import fs from 'node:fs';
import path from 'node:path';

import { RawDirectories } from '../types';

export function readDirectory(
    directory: string,
    depth: number
): RawDirectories[] {
    return fs
        .readdirSync(directory, {
            withFileTypes: true,
        })
        .map((file) => {
            const transformedFile: RawDirectories = {
                name: file.name,
                type: file.isDirectory() ? 'folder' : 'file',
            };

            if (file.isDirectory() && depth > 0) {
                transformedFile.children = readDirectory(
                    path.join(directory, file.name),
                    depth - 1
                );
            }

            return transformedFile;
        });
}
