#!/usr/bin/env node

import tre from '../lib/index.js';
import { Command } from 'commander';

const program = new Command();

program
    .argument('[directory]', 'the directory to build a tree from')
    .option('-L <level>', 'Max display depth of the directory tree')
    .option(
        '-a',
        'All files are printed.  By default ntree does not print ' +
            "hidden  files  (those beginning  with a dot `.').  In no event " +
            "does tree print the file system constructs `.' (current " +
            "directory) and `..' (previous directory)."
    )
    .option('-d', 'List directories only.')
    .option('-f', 'Prints the full path prefix for each file')
    .option(
        '-P <pattern>',
        'Show files which match the JavaScript regular expression'
    )
    .option(
        '-I <pattern>',
        'Show files which do not match the JavaScript regular expression'
    )
    .option(
        '--ignore-case',
        'If a match pattern is specified by the -P or -I option, this will ' +
            'cause the pattern to match without regards to the case of each letter.'
    )
    .option(
        '--filelimit <fileLimit>',
        'Do not descend directories that contain more than # entries.'
    )
    .action((directory, options) => {
        if (
            typeof options.L !== 'undefined' &&
            (Number.isNaN(Number(options.L)) || Number(options.L) === 0)
        ) {
            console.log('ntree: Invalid level, must be greater than 0.');
            process.exit(1);
        }
        const results = tre(directory, process.stdout, {
            depth:
                typeof options.L === 'undefined'
                    ? Number.POSITIVE_INFINITY
                    : Number(options.L) - 1,
            hidden: options.a ?? false,
            directoriesOnly: options.d ?? false,
            fullPath: options.f ?? false,
            pattern: options.P,
            antiPattern: options.I,
            fileLimit:
                typeof options.filelimit === 'undefined'
                    ? undefined
                    : Number(options.filelimit),
        });

        process.stdout.write(
            `\n${results.directories} directories, ${results.files} files\n`
        );
    });

program.parse(process.argv);
